cn = new Mongo();
db = cn.getDB('test');
print("insertMany sur collection étudiants");

print("insertMany sur collection profs");
db.profs.insertMany(
    [{
        _id: 0,
        nom: "Alan Turing"
    }, {
        _id: 1,
        nom: "John von Neumann"
    }]
);

var e = [{
    _id: 0,
    nom: "Alice",
    profs: [0, 1]
}, {
    _id: 1,
    nom: "Bob",
    profs:  [1, 2]
}, {
    _id: 2,
    nom: "Charles",
    profs: [0, 2]
}, {
    _id: 3,
    nom: "Dom",
    profs: [0, 1]
}];

db.etudiants.insertMany(
    e
);

printjson(db.etudiants.ensureIndex({
    'profs': 1
}));
print("Création d'un index à clés multiples sur la collection étudiants.")
db.etudiants.find({
    profs: {
        $all: [0, 1]
    }
}, {
    _id: 0,
    nom: 1
}).forEach(
    function(doc) {
        printjson(doc);
    }
);

print("Effacement des collections.")
db.etudiants.drop();
db.profs.drop();
