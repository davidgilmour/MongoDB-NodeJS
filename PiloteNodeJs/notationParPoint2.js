// utilisation :
// node notationParPoint2.js -f 2004 -l 2008 -e 100 -c IRL

var MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    cmd = require('command-line-args');

var options = optionsLigneCMD();

// gestion de la connexion à MongoDB
MongoClient.connect(
    'mongodb://localhost:27017/crunchbase',
    function(err, db) {

        assert.equal(err, null);
        console.log("Connexion réussie avec MongoDB.");

        // création dynamique de la requête
        var requete = requetageDocs(options);
        // création d'une projection
        var projection = {
            "_id": 0,
            "name": 1,
            "offices.country_code": 1
        };

        // gestion du curseur
        var curseur = db.collection('companies').find(requete, projection);
        // itération sur le curseur
        var compteur = 0;

        curseur.forEach(
            function(doc) {
                compteur = compteur + 1;
                console.log(doc);
            },
            // curseur vidé et erreurs
            function(err) {
                assert.equal(err, null);
                console.log("Requête : " + JSON.stringify(requete));
                console.log("Nombre de documents retournés : " + compteur);
                // fermeture de la base
                return db.close();
            }
        );

    }
);

// fonction de gestion des arguments passés en ligne de commande
function optionsLigneCMD() {

    var cli = cmd([{
        name: "firstYear",
        alias: "f",
        type: Number
    }, {
        name: "lastYear",
        alias: "l",
        type: Number
    }, {
        name: "employees",
        alias: "e",
        type: Number
    }, {
        name: "ipo",
        alias: "i",
        type: String
    }, {
        name: "country",
        alias: "c",
        type: String
    }]);

    var options = cli.parse();

    if (!(("firstYear" in options) && ("lastYear" in options))) {
        console.log(
            cli.getUsage({
                title: "Usage",
                description: "Deux options demandées ci-dessous sont obligatoires. Le reste est optionnel."
            }));
        process.exit();
    }

    return options;
}

// fonction de requêtage de documents
function requetageDocs(options) {
    // création directe d'un champs
    var requete = {
        "founded_year": {
            "$gte": options.firstYear,
            "$lte": options.lastYear
        }
    };

    // utilisation de la notation par point directement sur l'objet concerné
    if ("employees" in options) {
        requete.number_of_employees = {
            "$gte": options.employees
        };
    };

    // utilisation d'un tableau pour accéder à un document imbriqué
    if ("ipo" in options) {
        // options["ipo"] et options.ipo fonctionnent de la même façon
        if (options["ipo"] == "yes") {
            requete["ipo.valuation_amount"] = {
            // ceci ne fonctionnera pas : requete.ipo.valuation_amount, ni
            // requete.ipo["valuation_amount"]
                "$exists": true,
                "$ne": null
            };
        } else if (options["ipo"] == "no") {
            requete["ipo.valuation_amount"] = null;
        }
    }

    // notation par point dans un tableau de documents imbriqués
    // offices -> country_code
    if ("country" in options) {
        requete["offices.country_code"] = options.country;
    }

    return requete;
}
