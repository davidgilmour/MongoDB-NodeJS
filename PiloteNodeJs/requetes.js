var MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    cmd = require('command-line-args');

var options= optionsLigneCMD();

// gestion de la connexion à MongoDB
MongoClient.connect(
    'mongodb://localhost:27017/crunchbase',
    function(err, db) {


        assert.equal(err, null);
        console.log("Connexion réussie avec MongoDB.");

        // création dynamique de la requête
        var requete = requetageDocs(options);
        // création d'une projection
        var projection = {
            "_id": 0,
            "founded_year": 1,
            "number_of_employees": 1,
            "crunchbase_url": 1
        };

        // gestion du curseur
        var curseur = db.collection('companies').find(requete, projection);
        // itération sur le curseur
        var compteur = 0;

        curseur.forEach(
            function(doc) {
                compteur = compteur + 1;
                console.log(doc);
            },
            // curseur vidé et erreurs
            function(err) {
                assert.equal(err, null);
                console.log("Requête : " + JSON.stringify(requete));
                console.log("Nombre de documents retournés : " + compteur);
                // fermeture de la base
                return db.close();
            }
        );

    }
);

// fonction de gestion des arguments passés en ligne de commande
function optionsLigneCMD() {

    var cli = cmd([{
        name: "firstYear",
        alias: "f",
        type: Number
    }, {
        name: "lastYear",
        alias: "l",
        type: Number
    }, {
        name: "employees",
        alias: "e",
        type: Number
    }]);

    var options = cli.parse();

    if (!(("firstYear" in options) && ("lastYear" in options))) {
        console.log(
            cli.getUsage({
                title: "Usage",
                description: "Deux options demandées ci-dessous sont obligatoires. Le reste est optionnel."
            }));
        process.exit();
    }

    return options;
}

// fonction de requêtage de documents
function requetageDocs(options) {
    var requete = {
        "founded_year": {
            "$gte": options.firstYear,
            "$lte": options.lastYear
        }
    };

    if ("employees" in options) {
        requete.number_of_employees = {
            "$gte": options.employees
        };
    };

    return requete;
}
