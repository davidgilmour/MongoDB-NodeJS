// node sortEtLimit.js -f 2006 -l 2009 -e 250 --limit 10 --skip 0
// node sortEtLimit.js -f 2006 -l 2009 -e 250 --limit 10 --skip 10

var MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    cmd = require('command-line-args');

var options= optionsLigneCMD();

// gestion de la connexion à MongoDB
MongoClient.connect(
    'mongodb://localhost:27017/crunchbase',
    function(err, db) {

        assert.equal(err, null);
        console.log("Connexion réussie avec MongoDB.");

        // création dynamique de la requête
        var requete = requetageDocs(options);
        // création d'une projection
        var projection = {
            "_id": 0,
            "name":1,
            "founded_year": 1,
            "number_of_employees": 1,
        };

        // gestion du curseur
        var curseur = db.collection('companies').find(requete, projection);
        // tri multiple
        curseur.sort([
            ["founded_year", 1],["number_of_employees", -1]
        ]);

        // tri et limite
        curseur.skip(options.skip);
        curseur.limit(options.limit);

        // itération sur le curseur
        var compteur = 0;

        curseur.forEach(
            function(doc) {
                compteur = compteur + 1;
                console.log(doc.name+"\nDate de création:\t"+doc.founded_year+"\nNbre d'employés:\t"+doc.number_of_employees);
            },
            // curseur vidé et erreurs
            function(err) {
                assert.equal(err, null);
                console.log("Requête : " + JSON.stringify(requete));
                console.log("Nombre de documents retournés : " + compteur);
                // fermeture de la base
                return db.close();
            }
        );

    }
);

// fonction de gestion des arguments passés en ligne de commande
function optionsLigneCMD() {

    var cli = cmd([{
        name: "firstYear",
        alias: "f",
        type: Number
    }, {
        name: "lastYear",
        alias: "l",
        type: Number
    }, {
        name: "employees",
        alias: "e",
        type: Number
    }, {
        name: "limit",
        type: Number,
        defaultValue: 0
    },{
        name:"skip",
        type: Number,
        // la base comprends entre 18 et 19000 documents
        defaultValue: 200000
    }]);

    var options = cli.parse();

    if (!(("firstYear" in options) && ("lastYear" in options))) {
        console.log(
            cli.getUsage({
                title: "Usage",
                description: "Deux options demandées ci-dessous sont obligatoires. Le reste est optionnel."
            }));
        process.exit();
    }

    return options;
}

// fonction de requêtage de documents
function requetageDocs(options) {
    var requete = {
        "founded_year": {
            "$gte": options.firstYear,
            "$lte": options.lastYear
        }
    };

    if ("employees" in options) {
        requete.number_of_employees = {
            "$gte": options.employees
        };
    };

    return requete;
}
