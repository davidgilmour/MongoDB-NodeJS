var express = require('express'),
    app = express(),
    mongoClient =  require('mongodb').MongoClient,
    moteursDeTemplates = require('consolidate'),
    assert = require('assert');

app.engine('html', moteursDeTemplates.nunjucks);
app.set('view engine', 'html');
app.set('views', __dirname+'/views');

mongoClient.connect('mongodb://localhost:27017/video',
    function(err, db) {
        //erreur :
        assert.equal(null, err);

        //succès de connexion:
        console.log('Connecté avec succès au serveur MongoDB.');

        //interprète le template ./views/movies.html
        app.get('/', function(requete, reponse){
            db.collection('movies').find({}).toArray(function(err,doc) {
                reponse.render('movies', {'movies': doc});
            });
        });

        // pour toute autre route
        app.use(function(requete, reponse){
            reponse.sendStatus(404);
        });

        // lancement du serveur dans la fonction callback
        var serveur = app.listen(3000, function() {
            console.log("Serveur Express en écoute sur le port %s",
                serveur.address().port
            );
        });
    }
);
