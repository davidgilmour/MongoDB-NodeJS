var express = require('express'),
    app = express(),
    cons = require('consolidate');

app.engine('html', cons.nunjucks);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

// récupération d'une variable GET et de ses paramètres
app.get('/:nom', function(requete, reponse, suivant){
    var nom = requete.params.nom;
    var v1 = requete.query.param1;
    var v2 = requete.query.param2;
    reponse.render('hello', {nom: nom, v1: v1, v2: v2});
});

// gestion des erreurs internes du serveur
function gestErreur(err, req, rep, suiv) {
    console.error(err.message);
    console.error(err.stack);
    rep.status(500);
    rep.render('template_erreur', {erreur:err});
};

app.use(gestErreur);

var serveur = app.listen(3000, function(){
    console.log("Serveur Express écoutant le port %s", serveur.address().port);
});
