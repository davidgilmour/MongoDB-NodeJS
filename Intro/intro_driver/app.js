var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');

MongoClient.connect('mongodb://localhost:27017/video',
    function(err, db) {
        // en cas d'erreur
        assert.equal(null, err);

        // si la connexion est obtenue
        console.log("Connexion au serveur obtenue.");

        // récupération des documents
        db.collection('movies').find({}).toArray(function(err, docs){
            docs.forEach(function(doc){
                console.log(doc.title);
            });

            // fermeture de la Connexion
            db.close();
        });
        // résultat affiché après le message suivant
        console.log('find() a été appelé.');
    }
);
