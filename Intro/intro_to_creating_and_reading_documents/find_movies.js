use video;
db.movies.find();
db.movies.find({});

// utilisation de contraintes
db.movies.find({"year":1981}).pretty();
db.movies. find("title":"Jaws").pretty();

// curseur
var c = db.movies.find();
c.hasNext();
c.next();
c.next();
c.next();
c.hasNext();
