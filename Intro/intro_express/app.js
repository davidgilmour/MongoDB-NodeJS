var express = require('express'),
    app = express();

// création d'une route 'root'
app.get('/', function(requete, reponse) {
    reponse.send("Hello, world !");
});

// pour tout autre chemin : erreur 404
app.use(function(requete, reponse){
    reponse.sendStatus(404);
});

// création du serveur
var serveur = app.listen(3000, function(){
    console.log('Serveur Express en écoute sur le port : %s', serveur.address().port);
})
