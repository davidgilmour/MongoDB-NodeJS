var express = require('express'),
    app = express(),
    cons = require('consolidate'),
    bodyParser = require('body-parser');

// gestion des templates
app.engine('html', cons.nunjucks);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.use(bodyParser.urlencoded({extended: true}));

// gestion des erreurs internes
function gestErreur(err, req, rep, autre) {
    console.error(err.Message);
    console.error(err.Stack);
    rep.status(500).render('erreur_template', {erreur: err});
}

// gestion du GET
app.get('/', function(req, rep, next){
    rep.render('choixFruit',
        {'fruits': ['pomme', 'orange', 'banane', 'poire']}
    );
});

// gestion du POST
app.post('/choixFruit', function(req, rep, autre){
    var fav = req.body.fruit;
    if (typeof fav == 'undefined') {
        autre('Merci de choisir un fruit.');
    } else {
        rep.send("Votre fruit favori est "+ fav);
    }
});

app.use(gestErreur);

// lancement du serveur
var serveur = app.listen(3000, function() {
    console.log("Serveur Express écoutant sur le port %s", serveur.address().port);
});
