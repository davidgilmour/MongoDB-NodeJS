// .insertOne()
db.moviesScratch.insertOne({
    "title": "Rocky",
    "year": "1976",
    "imdb": "tt0075148"
});
db.moviesScratch.find().pretty()
db.moviesScratch.insertOne({
    "_id": "tt0075148",
    "title": "Rocky",
    "year": "1976"
});
db.moviesScratch.find().pretty()

// .insertMany()
db.moviesScratch.insertMany(
    [{
        "imdb": "tt0084726",
        "title": "Star Trek II: The Wrath of Khan",
        "year": 1982,
        "type": "movie"
    }, {
        "imdb": "tt0796366",
        "title": "Star Trek",
        "year": 2009,
        "type": "movie"
    }, {
        "imdb": "tt1408101",
        "title": "Star Trek Into Darkness",
        "year": 2013,
        "type": "movie"
    }, {
        "imdb": "tt0117731",
        "title": "Star Trek: First Contact",
        "year": 1996,
        "type": "movie"
    }]
);
// on supprime la collection
db.moviesScratch.drop();

//// paramètres acceptés par insertMany
//// présence d'un doc dupliqué avec un _id identique
db.moviesScratch.insertMany(
    [{
        "_id": "tt0084726",
        "title": "Star Trek II: The Wrath of Khan",
        "year": 1982,
        "type": "movie"
    }, {
        "_id": "tt0796366",
        "title": "Star Trek",
        "year": 2009,
        "type": "movie"
    }, {
        "_id": "tt0084726",
        "title": "Star Trek II: The Wrath of Khan",
        "year": 1982,
        "type": "movie"
    }, {
        "_id": "tt1408101",
        "title": "Star Trek Into Darkness",
        "year": 2013,
        "type": "movie"
    }, {
        "_id": "tt0117731",
        "title": "Star Trek: First Contact",
        "year": 1996,
        "type": "movie"
    }]
);

db.moviesScratch.drop();

//// ajout du paramètre ordered: bool
db.moviesScratch.insertMany(
    [{
        "_id": "tt0084726",
        "title": "Star Trek II: The Wrath of Khan",
        "year": 1982,
        "type": "movie"
    }, {
        "_id": "tt0796366",
        "title": "Star Trek",
        "year": 2009,
        "type": "movie"
    }, {
        "_id": "tt0084726",
        "title": "Star Trek II: The Wrath of Khan",
        "year": 1982,
        "type": "movie"
    }, {
        "_id": "tt1408101",
        "title": "Star Trek Into Darkness",
        "year": 2013,
        "type": "movie"
    }, {
        "_id": "tt0117731",
        "title": "Star Trek: First Contact",
        "year": 1996,
        "type": "movie"
    }], {
        "ordered": false
    }
);
