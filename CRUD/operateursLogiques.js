// $or : OU logique
db.movieDetails.find({
    $or: [{
        "tomato.meter": {
            $gt: 95
        }
    }, {
        "metacritic": {
            $gt: 88
        }
    }]
}).count();

//$and : ET logique

db.movieDetails.find({
    $and: [{
        "tomato.meter": {
            $gt: 95
        }
    }, {
        "metacritic": {
            $gt: 88
        }
    }]
}).count();

// ici $and est superlfu
db.movieDetails.find({
    "tomato.meter": {
        $gt: 95
    },
    "metacritic": {
        $gt: 88
    }
}).count()
