// requête sur des documents imbriqués
db.toyStory.insertOne({
    "_id": ObjectId("5692a15524de1e0ce2dfcfa3"),
    "title": "Toy Story 3",
    "year": 2010,
    "rated": "G",
    "released": ISODate("2010-06-18T04:00:00Z"),
    "runtime": 103,
    "countries": [
        "USA"
    ],
    "genres": [
        "Animation",
        "Adventure",
        "Comedy"
    ],
    "director": "Lee Unkrich",
    "writers": [
        "John Lasseter",
        "Andrew Stanton",
        "Lee Unkrich",
        "Michael Arndt"
    ],
    "actors": [
        "Tom Hanks",
        "Tim Allen",
        "Joan Cusack",
        "Ned Beatty"
    ],
    "plot": "The toys are mistakenly delivered to a day-care center instead of the attic right before Andy leaves for college, and it's up to Woody to convince the other toys that they weren't abandoned and to return home.",
    "poster": "http://ia.media-imdb.com/images/M/MV5BMTgxOTY4Mjc0MF5BMl5BanBnXkFtZTcwNTA4MDQyMw@@._V1_SX300.jpg",
    "imdb": {
        "id": "tt0435761",
        "rating": 8.4,
        "votes": 500084
    },
    "tomato": {
        "meter": 99,
        "image": "certified",
        "rating": 8.9,
        "reviews": 287,
        "fresh": 283,
        "consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
        "userMeter": 89,
        "userRating": 4.3,
        "userReviews": 602138
    },
    "metacritic": 92,
    "awards": {
        "wins": 56,
        "nominations": 86,
        "text": "Won 2 Oscars. Another 56 wins & 86 nominations."
    },
    "type": "movie"
});

// requête sur un champs d'un document imbriqué
db.toyStory.find({
    "tomato.rating": 8.9
}).count()

// recherche sur un tableau : correspondance exacte
db.toyStory.find({
    "actors": ["Tom Hanks", "Tim Allen", "Joan Cusack", "Ned Beatty"]
}).count();
// -> 1 doc trouvé
db.toyStory.find({
    "actors": ["Tim Allen", "Tom Hanks", "Joan Cusack", "Ned Beatty"]
}).count();
// -> aucun document trouvé

// recherche sur un élément d'un tableau
db.toyStory.find({
    "actors": "Ned Beatty"
});

// recherche sur une position d'un tableau pour une valeur donnée
db.toyStory.find({
    "actors.1": "Tim Allen"
}).count();
// résultat = 1
db.toyStory.find({
    "actors.3": "Tim Allen"
}).count();
// résultat = 0

// Curseurs
var c = db.toyStory.find();

var doc = function() {
    return c.hasNext() ? c.next() : null;
};
doc();
doc();

// combien reste-t-il d'éléments à parcourir
c.objsLeftInBatch()

// Projections
// affichage du titre (_id affiché par défaut)
db.toyStory.find({
    "rated": "G"
}, {
    title: 1
});

// affichage du titre sans _id
db.toyStory.find({
    "rated": "G"
}, {
    title: 1, _id: 0
});

// affichage de certains éléments

db.toyStory.find({
    "rated": "G"
}, {
    title: 1, _id: 0, year: 1
});
