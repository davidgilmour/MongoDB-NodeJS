// opérateur $all
db.movieDetails.find({
    "genres": {
        $all: ["Comedy", "Crime", "Drama"]
    }
}, {
    "_id": 0,
    title: 1,
    genres: 1
});

//opérateur $size
db.movieDetails.find({
    countries: {
        $size: 1
    }
}).count();

//opérateur $elemMatch
use test;
db.movieDetails.insertMany([{
    boxOffice: [{
        "country": "USA",
        "revenue": 41.3
    }, {
        "country": "Australia",
        "revenue": 2.9
    }, {
        "country": "UK",
        "revenue": 10.1
    }, {
        "country": "Germany",
        "revenue": 4.3
    }, {
        "country": "France",
        "revenue": 3.5
    }]
}, {
    boxOffice: [{
        "country": "USA",
        "revenue": 101.4
    }, {
        "country": "Australia",
        "revenue": 4.9
    }, {
        "country": "UK",
        "revenue": 5.5
    }, {
        "country": "Germany",
        "revenue": 9.6
    }, {
        "country": "France",
        "revenue": 3.9
    }]
}, {
    boxOffice: [{
        "country": "USA",
        "revenue": 55.3
    }, {
        "country": "Australia",
        "revenue": 7.1
    }, {
        "country": "UK",
        "revenue": 23.5
    }, {
        "country": "Germany",
        "revenue": 5.7
    }, {
        "country": "France",
        "revenue": 21.3
    }, {
        "country": "Japan",
        "revenue": 10.5
    }]
}]);

db.movieDetails.find({
    "boxOffice": {
        $elemMatch: {
            country: "USA",
            revenue: {
                $gt: 55
            }
        }
    }
}).count();

db.movieDetails.drop();
